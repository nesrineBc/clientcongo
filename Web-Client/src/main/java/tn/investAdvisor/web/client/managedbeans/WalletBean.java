package tn.investAdvisor.web.client.managedbeans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.contracts.Operation.IManagementOperationLocal;
import tn.esprit.investadvisor.contracts.Securities.IManagementSecuritiesLocal;
import tn.esprit.investadvisor.contracts.wallet.IManagementWalletLocal;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Operation;
import tn.esprit.investadvisor.entities.OperationPk;
import tn.esprit.investadvisor.entities.Securities;
import tn.esprit.investadvisor.entities.State;
import tn.esprit.investadvisor.entities.Wallet;
import tn.esprit.investadvisor.entities.Wallet_Securities;

@ManagedBean
@RequestScoped
public class WalletBean {
	private Client client;
	private Wallet wallet;
	private Integer buyInt;
	private Integer cancelId;
	private String[] sellSec;

	public String[] getSellSec() {
		return sellSec;
	}

	public void setSellSec(String[] sellSec) {
		this.sellSec = sellSec;
	}

	public Integer getBuyInt() {
		return buyInt;
	}

	public void setBuyInt(Integer buyInt) {
		this.buyInt = buyInt;
	}

	public Integer getCancelId() {
		return cancelId;
	}

	public void setCancelId(Integer cancelId) {
		this.cancelId = cancelId;
	}

	private List<Operation> operations;

	private List<Operation> operationsOnSell;

	public List<Operation> getOperationsOnSell() {
		return operationsOnSell;
	}

	public void setOperationsOnSell(List<Operation> operationsOnSell) {
		this.operationsOnSell = operationsOnSell;
	}

	private List<Securities> securitiesWallet;
	private List<Wallet_Securities> wallet_Securities;

	public List<Wallet_Securities> getWallet_Securities() {
		return wallet_Securities;
	}

	public void setWallet_Securities(List<Wallet_Securities> wallet_Securities) {
		this.wallet_Securities = wallet_Securities;
	}

	public List<Securities> getSecuritiesWallet() {
		return securitiesWallet;
	}

	public void setSecuritiesWallet(List<Securities> securitiesWallet) {
		this.securitiesWallet = securitiesWallet;
	}

	private Securities security;

	public Securities getSecurity() {
		return security;
	}

	public void setSecurity(Securities security) {
		this.security = security;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

	@EJB
	private IManagementClientLocal ciClientLocal;
	@EJB
	private IManagementOperationLocal ciOperationLocal;
	@EJB
	private IManagementWalletLocal ciWalletLocal;
	@EJB
	private IManagementSecuritiesLocal ciSecurities;
	private List<Securities> securities;

	@PostConstruct
	public void init() {

		client = ciClientLocal.findClientById(1);
		System.out.println(client);
		operations = ciOperationLocal.findOperationByClient(client);
		securitiesWallet = ciWalletLocal.findSecuritiesByWallet(client
				.getWallet().getId());
		wallet_Securities = ciWalletLocal.findAllSecurities_Wallet();
		operationsOnSell = new ArrayList<Operation>();
		for (int f = 0; f < operations.size(); f++) {
			if (operations.get(f).getState() == State.OnSell) {
				operationsOnSell.add(operations.get(f));
			}
		}
		client.getWallet().setSecuritiesValue((float) 0);
		for (int i = 0; i < securitiesWallet.size(); i++) {
			client.getWallet().setSecuritiesValue(
					client.getWallet().getSecuritiesValue()
							+ securitiesWallet.get(i).getClosingPrice()
							* doFindBysecurityId(i));
		}
		client.getWallet().setWalletValue(
				client.getWallet().getRiskfree()
						+ client.getWallet().getSecuritiesValue());
		ciClientLocal.updateClient(client);
		sellSec = new String[securitiesWallet.size()];
	}

	public List<Securities> getSecurities() {
		return securities;
	}

	public void setSecurities(List<Securities> securities) {
		this.securities = securities;
	}

	public Integer doFindBysecurityId(Integer j) {
		for (int i = 0; i < wallet_Securities.size(); i++) {
			if ((wallet_Securities.get(i).getSecurities().getId() == j)
					&& (wallet_Securities.get(i).getWallet().getId() == client
							.getWallet().getId())) {
				return (wallet_Securities.get(i).getQuantity());
			}
		}
		return 0;
	}

	public void doSell(Securities secse, Integer quantity) {
		System.out.println("551");

		List<Operation> secses = ciOperationLocal.findAll();
		List<Operation> secOnSell = new ArrayList<Operation>();
		Wallet_Securities walletsecupdate;
		for (int i = 0; i < secses.size(); i++) {
			if (secses.get(i).getState() == State.OnSell) {
				secOnSell.add(operations.get(i));
			}
		}
		for (int i = 0; i < secOnSell.size(); i++) {
			if (secOnSell.get(i).getQuantity() >= quantity) {
				secOnSell.get(i).setQuantity(
						secOnSell.get(i).getQuantity() - quantity);
				walletsecupdate = ciWalletLocal.findSecWByid(secOnSell.get(i)
						.getWallet().getId(), secOnSell.get(i).getSecurities()
						.getId());
				walletsecupdate.setQuantity(ciWalletLocal.findSecWByid(
						secOnSell.get(i).getWallet().getId(),
						secOnSell.get(i).getSecurities().getId()).getQuantity()
						- quantity);
				ciWalletLocal.updateSECWALLET(walletsecupdate);
				Wallet walletvendeur = ciWalletLocal.findWalletById(secOnSell
						.get(i).getOperationPk().getWalletId());
				walletvendeur.setRiskfree(walletvendeur.getRiskfree()
						+ quantity * secse.getClosingPrice());
				ciWalletLocal.updateWallet(walletvendeur);
				if (secOnSell.get(i).getQuantity() == 0) {
					ciOperationLocal.deleteOperation(secOnSell.get(i));
				} else {
					ciWalletLocal.findSecWByid(
							secOnSell.get(i).getWallet().getId(),
							secOnSell.get(i).getSecurities().getId())
							.setQuantity(
									ciWalletLocal.findSecWByid(
											secOnSell.get(i).getWallet()
													.getId(),
											secOnSell.get(i).getSecurities()
													.getId()).getQuantity()
											- quantity);
					ciOperationLocal.updateOperation(secOnSell.get(i));
				}
				break;
			} else {
				Wallet walletvendeur = ciWalletLocal.findWalletById(secOnSell
						.get(i).getOperationPk().getWalletId());
				walletvendeur.setRiskfree(walletvendeur.getRiskfree()
						+ secOnSell.get(i).getQuantity()
						* secse.getClosingPrice());
				ciWalletLocal.updateWallet(walletvendeur);
				walletsecupdate = ciWalletLocal.findSecWByid(secOnSell.get(i)
						.getWallet().getId(), secOnSell.get(i).getSecurities()
						.getId());
				ciWalletLocal.deleteSECWALLET(walletsecupdate);
				quantity = quantity - secOnSell.get(i).getQuantity();
				ciOperationLocal.deleteOperation(secOnSell.get(i));
			}
		}
	}

	public void doBuy(Securities secse) {
		OperationPk operationPk = new OperationPk();
		operationPk.setSecuritiesId(secse.getId());
		operationPk.setWalletId(client.getWallet().getId());
		Calendar c = Calendar.getInstance();
		Date date = new Date(c.getTime().getTime());
		System.out.println(date);
		operationPk.setOperationDate(date);
		Operation op = new Operation();
		op.setOperationPk(operationPk);
		op.setQuantity(Integer.parseInt(sellSec[secse.getId() - 1]));
		op.setSecurities(secse);
		op.setState(State.Bought);
		op.setTransactionFee(secse.getClosingPrice()
				* Integer.parseInt(sellSec[secse.getId() - 1]));
		op.setWallet(client.getWallet());
		boolean containss = false;
		Integer addd = Integer.parseInt(sellSec[secse.getId() - 1]);
		for (int i = 0; i < wallet_Securities.size(); i++) {
			if (wallet_Securities.get(i).getWallet_SecuritiesPK()
					.getSecuritiesId() == secse.getId()) {
				containss = true;
				addd = Integer.parseInt(sellSec[secse.getId() - 1])
						+ wallet_Securities.get(i).getQuantity();
			}
		}
		ciWalletLocal.AssignSecuritiesToWallet(client.getWallet(), secse, addd);
		ciOperationLocal.addOperation(op);
		client.getWallet().setRiskfree(
				client.getWallet().getRiskfree()
						- (Integer.parseInt(sellSec[secse.getId() - 1]) * secse
								.getClosingPrice()));
		client.getWallet().setSecuritiesValue(
				client.getWallet().getRiskfree()
						- (Integer.parseInt(sellSec[secse.getId() - 1]) * secse
								.getClosingPrice()));
		ciWalletLocal.updateWallet(client.getWallet());
		secse.setTradedQuantity(secse.getTradedQuantity()
				- Integer.parseInt(sellSec[secse.getId() - 1]));
		ciSecurities.updateSecurities(secse);
		// doSell(secse, Integer.parseInt(sellSec[secse.getId()-1]));
	}

	public void doPutOnSell(Securities secse) {
		OperationPk operationPk = new OperationPk();
		operationPk.setSecuritiesId(secse.getId());
		operationPk.setWalletId(client.getWallet().getId());
		Calendar c = Calendar.getInstance();
		Date date = new Date(c.getTime().getTime());
		System.out.println(date);
		operationPk.setOperationDate(date);
		Operation op = new Operation();
		op.setOperationPk(operationPk);
		op.setQuantity(Integer.parseInt(sellSec[secse.getId() - 1]));
		op.setSecurities(secse);
		op.setState(State.OnSell);
		op.setTransactionFee(secse.getClosingPrice()
				* Integer.parseInt(sellSec[secse.getId() - 1]));
		op.setWallet(client.getWallet());
		ciOperationLocal.addOperation(op);
		sellSec[secse.getId() - 1] = "";
	}

	public void doCanceSell(Operation o) {
		ciOperationLocal.deleteOperation(o);
	}
}