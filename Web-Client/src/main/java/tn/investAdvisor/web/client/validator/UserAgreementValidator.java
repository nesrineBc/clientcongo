package tn.investAdvisor.web.client.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("userAgreementValidator")
public class UserAgreementValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		UIInput input = (UIInput) component.getAttributes()
				.get("userAgreement");
		Boolean submittedValue = (Boolean) input.getSubmittedValue();
		if (!submittedValue) {
			throw new ValidatorException(
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Please accept the agreement",
							"You should read and accept the user agreement before submitting the form"));
		}

	}
}
