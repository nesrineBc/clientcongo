package tn.investAdvisor.web.client.managedbeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.contracts.Operation.IManagementOperationLocal;
import tn.esprit.investadvisor.contracts.wallet.IManagementWalletLocal;
import tn.esprit.investadvisor.entities.Client;
import tn.esprit.investadvisor.entities.Operation;


@ManagedBean
@RequestScoped
public class Wallet {
	private Client client;
	private Wallet wallet;
	private List<Operation> operations;
	public List<Operation> getOperations() {
		return operations;
	}
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Wallet getWallet() {
		return wallet;
	}
	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	} 
	
	@EJB
	private IManagementClientLocal ciClientLocal; 
	@EJB
	private IManagementOperationLocal ciOperationLocal;
	@EJB
	private IManagementWalletLocal ciWalletLocal;
	@PostConstruct
	public void init(){
		client=ciClientLocal.findClientById(1);
		System.out.println(client);
		operations=ciOperationLocal.findOperationByClient(client);
		System.out.println("abcd");
	}
}
