package tn.investAdvisor.web.client.convertor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;



import tn.esprit.investadvisor.contracts.Currency.IManagementCurrencyLocal;
import tn.esprit.investadvisor.entities.Currency;

@SessionScoped
@ManagedBean(name = "Currencytest")
public class CurrencyTestBean {

	private List<Currency> currencyList = new ArrayList<Currency>();
	private Currency currency = new Currency();
	private Integer id;
	private String name;
	private String logo;
	
	@EJB
	IManagementCurrencyLocal currencyService;
	
	

	public List<Currency> getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(List<Currency> currencyList) {
		this.currencyList = currencyList;
	}

	

	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String doUpdateCurrency() {
		try {
						
			currencyService.updateCurrency(currency);

			MessageUtil.addSuccessMessage("Post was successfully created."
					+ this.currency.getId());
		} catch (Exception e) {
			MessageUtil
					.addErrorMessage("Post could not be saved. A Persisting error occured."
							+ this.currency.getId());
		}

		return null;
	}

	public void doLoadCurrenies() {
		this.currencyList =currencyService.retrieveAllCurrency() ;
	}

	public void doFindCurrency() {

		
		this.currency=currencyService.retrieveCurrency(currency.getId()) ;
	}

	public String doDeleteCurrency(Currency currency) {

		try {

			currencyService.deleteCurrency(currency);
			MessageUtil.addSuccessMessage("Post was successfully deleted.");
		} catch (Exception e) {
			MessageUtil.addErrorMessage("Could not delete currency.");
		}

		return null;
	}

}
