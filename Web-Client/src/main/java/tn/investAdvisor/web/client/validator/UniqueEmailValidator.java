package tn.investAdvisor.web.client.validator;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;

@RequestScoped
@ManagedBean(name = "uniqueEmail")
public class UniqueEmailValidator implements Validator {

	@EJB
	IManagementClientLocal clientServices;

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String email = (String) value;
		if (clientServices.findClientByEmailBo(email)) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Email already exists",
					"Please choose another email"));
		}
	}

}
