package tn.investAdvisor.web.client.convertor;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import tn.esprit.investadvisor.contracts.BankCompany.IManagementBankCompanyLocal;
import tn.esprit.investadvisor.entities.BankCompany;
import tn.esprit.investadvisor.entities.TunisanBankCurrency;


@ManagedBean
@RequestScoped
public class CurrencyMangeBean {
	private BankCompany bCampany;
	private List<TunisanBankCurrency> lCureencies;
	private Integer test;
	private Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
	private String idB;
	public Integer getTest() {
		return test;
	}
	public void setTest(Integer test) {
		this.test = test;
	}
	@EJB
	private IManagementBankCompanyLocal cinBankCompany;
	public BankCompany getbCampany() {
		return bCampany;
	}
	public void setbCampany(BankCompany bCampany) {
		this.bCampany = bCampany;
	}
	public List<TunisanBankCurrency> getlCureencies() {
		return lCureencies;
	}
	public void setlCureencies(List<TunisanBankCurrency> lCureencies) {
		this.lCureencies = lCureencies;
	}
	@PostConstruct
	public void init(){
		test = 5;
		idB=params.get("bankCompanyId");
		System.out.println(idB);
		bCampany=cinBankCompany.findBankCompanyById(Integer.parseInt(idB));
		lCureencies=bCampany.getTunisianBankCurrencies();
	}
}
