package tn.investAdvisor.web.client.convertor;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.investadvisor.contracts.Securities.IManagementSecuritiesLocal;
import tn.esprit.investadvisor.entities.Securities;




@SessionScoped
@ManagedBean(name = "Security")
public class SecuritiesBean {
	
	
	

		private List<Securities> securitiesList = new ArrayList<Securities>();
		private Securities security = new Securities() {};
		private Integer id;
		private String value;
		private Float OpeningValue; //ouverture
		private Date session;
		private Float capital; //volume en dt
		private Float lastPrice; // dernier
		private Float closingPrice; 
		private Float variabiliy; // var
		//Integer quantity;
		private Float highest;//haut
		private Float lowest; // bas
		private Integer tradedQuantity; //volume en titre
		
		@EJB
		IManagementSecuritiesLocal securityService ;

		

		public List<Securities> getSecuritiesList() {
			return securitiesList;
		}

		public void setSecuritiesList(List<Securities> securitiesList) {
			this.securitiesList = securitiesList;
		}

		public Securities getSecurity() {
			return security;
		}

		public void setSecurity(Securities security) {
			this.security = security;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Float getOpeningValue() {
			return OpeningValue;
		}

		public void setOpeningValue(Float openingValue) {
			OpeningValue = openingValue;
		}

		public Date getSession() {
			return session;
		}

		public void setSession(Date session) {
			this.session = session;
		}

		public Float getCapital() {
			return capital;
		}

		public void setCapital(Float capital) {
			this.capital = capital;
		}

		public Float getLastPrice() {
			return lastPrice;
		}

		public void setLastPrice(Float lastPrice) {
			this.lastPrice = lastPrice;
		}

		public Float getClosingPrice() {
			return closingPrice;
		}

		public void setClosingPrice(Float closingPrice) {
			this.closingPrice = closingPrice;
		}

		public Float getVariabiliy() {
			return variabiliy;
		}

		public void setVariabiliy(Float variabiliy) {
			this.variabiliy = variabiliy;
		}

		public Float getHighest() {
			return highest;
		}

		public void setHighest(Float highest) {
			this.highest = highest;
		}

		public Float getLowest() {
			return lowest;
		}

		public void setLowest(Float lowest) {
			this.lowest = lowest;
		}

		public Integer getTradedQuantity() {
			return tradedQuantity;
		}

		public void setTradedQuantity(Integer tradedQuantity) {
			this.tradedQuantity = tradedQuantity;
		}

		public IManagementSecuritiesLocal getSecurityService() {
			return securityService;
		}

		public void setSecurityService(IManagementSecuritiesLocal securityService) {
			this.securityService = securityService;
		}

		public String doUpdateCurrency() {
			try {
							
				
				securityService.updateSecurities(security);

				MessageUtil.addSuccessMessage("Post was successfully created."
						+ this.security.getId());
			} catch (Exception e) {
				MessageUtil
						.addErrorMessage("Post could not be saved. A Persisting error occured."
								+ this.security.getId());
			}

			return null;
		}

		public void doLoadSecurities() {
			
			this.securitiesList=securityService.findAll() ;
		}

		public void doFindSecurity() {

			
			
			this.security=securityService.findSecuritiesById(security.getId()) ;
		}

		public String doDeleteSecurity(Securities security) {

			try {

				
				securityService.deleteSecurities(security);
				MessageUtil.addSuccessMessage("Post was successfully deleted.");
			} catch (Exception e) {
				MessageUtil.addErrorMessage("Could not delete currency.");
			}

			return null;
		}


}
