package tn.investAdvisor.web.client.convertor;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


import tn.esprit.investadvisor.entities.TunisanBankCurrency;

@SessionScoped
@ManagedBean(name = "TunisianCurrency")
public class TunisianCurrencyBean {
	private List<TunisanBankCurrency> tunisanBankCurrencies = new ArrayList<TunisanBankCurrency>();
	private TunisanBankCurrency tunisanBankCurrency = new TunisanBankCurrency();
	private Integer id;
	private String name;
	private String logo;
	

}
