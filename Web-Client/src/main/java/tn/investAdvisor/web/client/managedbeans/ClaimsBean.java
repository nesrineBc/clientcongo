package tn.investAdvisor.web.client.managedbeans;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsLocal;
import tn.esprit.investadvisor.contracts.Client.IManagementClientLocal;
import tn.esprit.investadvisor.entities.Claims;


@RequestScoped
@ManagedBean(name="claims")
public class ClaimsBean {

	@EJB
	ManagementClaimsLocal claimsServices;
	@EJB
	IManagementClientLocal clientservice;
	
	private Claims claim=new Claims();
	public Claims getClaim() {
		return claim;
	}

	public void setClaim(Claims claim) {
		this.claim = claim;
	}

	private List<Claims> claims;
	
	public List<Claims> getClaims() {
		return claims;
	}

	public void setClaims(List<Claims> claims) {
		this.claims = claims;
	}

	
	
	@PostConstruct
	public void init(){
	claims=claimsServices.findclaimsByUserId(2);
	}
	
	public String doAddClaims()
	{
		claim.setUserFrom(0);
		claim.setClient(clientservice.findClientById(2));
		claim.setDate_claim(new Date());
		claimsServices.addClaims(claim);
		return "/Client/claimsRec?faces-redirect=true";
	}
	
}
