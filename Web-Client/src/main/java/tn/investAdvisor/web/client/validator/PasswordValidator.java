package tn.investAdvisor.web.client.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("passwordValidator")
public class PasswordValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String password = (String) value;
		UIInput confirmPassword = (UIInput) component.getAttributes().get(
				"confirmPassword");
		String confirmationPassword = (String) confirmPassword
				.getSubmittedValue();
		if (password.equals(confirmationPassword)) {
			return;
		} else {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Passwords error",
					"Passwords are not equal"));
		}

	}
}
