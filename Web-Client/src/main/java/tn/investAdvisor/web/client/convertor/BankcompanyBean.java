package tn.investAdvisor.web.client.convertor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import tn.esprit.investadvisor.contracts.BankCompany.IManagementBankCompanyLocal;
import tn.esprit.investadvisor.contracts.Currency.IManagementCurrencyLocal;
import tn.esprit.investadvisor.contracts.TunisianBankCurrency.IManagementTunisianBankCurrencyLocal;
import tn.esprit.investadvisor.entities.BankCompany;
import tn.esprit.investadvisor.entities.Currency;
import tn.esprit.investadvisor.entities.TunisanBankCurrency;

@RequestScoped
@ManagedBean(name = "BankCompany")
public class BankcompanyBean {
	
	private List<BankCompany> bankcompanies = new ArrayList<BankCompany>();
	private BankCompany bankCompany=new BankCompany() ;
	private TunisanBankCurrency tunisanBankCurrency=new TunisanBankCurrency();
	private List<TunisanBankCurrency> tunisanBankCurrencies = new ArrayList<TunisanBankCurrency>();
	private List<TunisanBankCurrency> tunisanBankCurrencies1 = new ArrayList<TunisanBankCurrency>();
	
	private List<Currency> currencyList = new ArrayList<Currency>();
	private List<Currency> currencyList1 = new ArrayList<Currency>();
	
	
	
	public List<Currency> getCurrencyList() {
		return currencyList; }

	public void setCurrencyList(List<Currency> currencyList) {
		this.currencyList = currencyList;
	}

	public List<Currency> getCurrencyList1() {
		return currencyList1;
	}

	public void setCurrencyList1(List<Currency> currencyList1) {
		this.currencyList1 = currencyList1;
	}

	public List<TunisanBankCurrency> getTunisanBankCurrencies() {
		return tunisanBankCurrencies;
	}

	public void setTunisanBankCurrencies(
			List<TunisanBankCurrency> tunisanBankCurrencies) {
		this.tunisanBankCurrencies = tunisanBankCurrencies;
	}

	public List<TunisanBankCurrency> getTunisanBankCurrencies1() {
		return tunisanBankCurrencies1;
	}

	public void setTunisanBankCurrencies1(
			List<TunisanBankCurrency> tunisanBankCurrencies1) {
		this.tunisanBankCurrencies1 = tunisanBankCurrencies1;
	}

	private Integer id;
	private String name;
	private String phone;
	private String address;
	private String leadership;
	private String Shares; 
	private String floating;
	private String TotalCapital;
	private String description;
	private Map<String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
	public Map<String, String> getParams() {
		return params;
	}
	@PostConstruct
	public void init(){
		doLoadBankCompany();
		
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	private String bId=params.get("bankId");
	public String getbId() {
		return bId;
	}

	public void setbId(String bId) {
		this.bId = bId;
	}

	@EJB
	IManagementBankCompanyLocal bankcompanyService;
	@EJB
	IManagementTunisianBankCurrencyLocal currencyService;
	@EJB
	IManagementCurrencyLocal currencyService1New ;

	public List<BankCompany> getBankcompanies() {
		return bankcompanies;
	}

	public void setBankcompanies(List<BankCompany> bankcompanies) {
		this.bankcompanies = bankcompanies;
	}

	public BankCompany getBankCompany() {
		return bankCompany;
	}

	public void setBankCompany(BankCompany bankCompany) {
		this.bankCompany = bankCompany;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLeadership() {
		return leadership;
	}

	public void setLeadership(String leadership) {
		this.leadership = leadership;
	}

	public String getShares() {
		return Shares;
	}

	public void setShares(String shares) {
		Shares = shares;
	}

	public String getFloating() {
		return floating;
	}

	public void setFloating(String floating) {
		this.floating = floating;
	}

	public String getTotalCapital() {
		return TotalCapital;
	}

	public void setTotalCapital(String totalCapital) {
		TotalCapital = totalCapital;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
	

	public String doUpdateBankCompany() {
		try {
						
			
			bankcompanyService.updateBankCompany(bankCompany);

			MessageUtil.addSuccessMessage("Post was successfully created."
					+ this.bankCompany.getId());
		} catch (Exception e) {
			MessageUtil
					.addErrorMessage("Post could not be saved. A Persisting error occured."
							+ this.bankCompany.getId());
		}

		return null;
	}

	public void doLoadBankCompany() {
		
		this.bankcompanies=bankcompanyService.findAll();
	}

	public void doFindBankCompany() {
		
		
		/*	int tId;
		if(bId==null)
			bId="mafamma shayy";
		tId=Integer.parseInt(bId);*/
		this.bankCompany=bankcompanyService.findBankCompanyById(bankCompany.getId()) ;
	/*	tunisanBankCurrencies=currencyService.findAll();
		
		for(int i=0;i<tunisanBankCurrencies.size();i++)
		{
			if(tunisanBankCurrencies.get(i).getBankCompany().equals(bankCompany)){
				tunisanBankCurrencies1.add(tunisanBankCurrencies.get(i));
			}
		}
		*/
		//	System.out.println(bankCompany.getId());
		
	}

	public String doDeleteBankCompany(BankCompany bankCompany) {

		try {

			
			bankcompanyService.deleteBankCompany(bankCompany);
			
			MessageUtil.addSuccessMessage("Post was successfully deleted.");
		} catch (Exception e) {
			MessageUtil.addErrorMessage("Could not delete currency.");
		}

		return null;
	}

	public TunisanBankCurrency getTunisanBankCurrency() {
		return tunisanBankCurrency;
	}

	public void setTunisanBankCurrency(TunisanBankCurrency tunisanBankCurrency) {
		this.tunisanBankCurrency = tunisanBankCurrency;
	}

}
