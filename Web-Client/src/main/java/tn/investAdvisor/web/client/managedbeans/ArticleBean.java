package tn.investAdvisor.web.client.managedbeans;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

import tn.esprit.investadvisor.contracts.Claims.ManagementClaimsLocal;
import tn.esprit.investadvisor.contracts.StockHistorization.IManagementStockHistorizationLocal;
import tn.esprit.investadvisor.contracts.StockHistorization.IManagementStockHistorizationRemote;
import tn.esprit.investadvisor.entities.Fields;



@ManagedBean
@RequestScoped
public class ArticleBean {

	
	@EJB
	IManagementStockHistorizationLocal articServices;
	
	List<Fields> articles=new ArrayList<Fields>();
	
	public List<Fields> getArticles() {
		return articles;
	}

	public void setArticles(List<Fields> articles) {
		this.articles = articles;
	}

	@PostConstruct
	public void init(){
		List<Fields> articlehhs=new ArrayList<Fields>();
		articlehhs=articServices.findAllField();
		
		for(int k=0;k<10;k++){
		int max=0;
		int j=0;
		for(int i=0;i<articlehhs.size();i++){
			String ch=articlehhs.get(i).getDate();
			
	Date date=new Date(Integer.parseInt("20"+ch.substring(6, 8)),Integer.parseInt(ch.substring(3,5)),Integer.parseInt(ch.substring(0, 2)),Integer.parseInt(ch.substring(9, 11)),Integer.parseInt(ch.substring(12)));
	
	
	if(max<1000*date.getYear()+100*date.getMonth()+date.getDay()){
		max=max=1000*date.getYear()+100*date.getMonth()+date.getDay();
		j=i;
	}
	
		}
		articles.add(articlehhs.get(j));	
		articlehhs.remove(j);
		
		}
		
	}
}
